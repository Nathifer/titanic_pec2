#!/usr/bin/env python
# coding: utf-8

# In[2]:


# data manipulation and plotting
import pandas as pd
import numpy as np

# for saving the pipeline
import joblib

# from Scikit-learn
from sklearn.pipeline import Pipeline

# from feature-engine
from feature_engine.imputation import (
    MeanMedianImputer,
    AddMissingIndicator,
    CategoricalImputer
)

from feature_engine.selection import DropFeatures

from sklearn.preprocessing import OneHotEncoder

#to separate training and test
from sklearn.model_selection import train_test_split

#the model
from sklearn.linear_model import LogisticRegression


# In[5]:


# CARGAR DATA
file_name = "/home/nathalia//Documentos/analitica_escalable/bloque_2/titanic(2).csv"
df = pd.read_csv(file_name, sep =";")
df.head()


# In[6]:


X = df[["pclass", "name", "sex", "age", "sibsp", "parch", "ticket","fare", "cabin", "embarked"]]
Y = df["survived"]


# In[7]:


# Let's separate into train and test set

X_train, X_test, Y_train, Y_test = train_test_split(
    X, #features
    Y, #labels
    test_size=0.3, #portion to test
    random_state=42 #seed definition
)

X_train = pd.DataFrame(X_train, columns = ["pclass", "name", "sex", "age", "sibsp", "parch", "ticket","fare", "cabin", "embarked"])
X_test = pd.DataFrame(X_test, columns = ["pclass", "name", "sex", "age", "sibsp", "parch", "ticket","fare", "cabin", "embarked"])
Y_train = pd.DataFrame(Y_train, columns = ["survived"])
Y_test = pd.DataFrame(Y_test, columns = ["survived"])


# In[8]:


df.isnull().sum() / len(df) * 100


# In[123]:


df.dtypes


# In[9]:


#CONFIGURATION

# numerical variables with NA in train set
NUMERICAL_VARS_WITH_NA = ['age']

# categorical variables with NA in train set
CATEGORICAL_VARS_WITH_NA = ['embarked']

# Drop features
DROP_FEATURES = ["name",  "ticket", "cabin"]

# the selected variables
FEATURES = ["pclass", "sex", "age", "sibsp", "parch","fare", "embarked"]


# In[10]:


# set up the pipeline
survived_pipe = Pipeline([

    # ===== IMPUTATION =====
    ('drop_features', DropFeatures(features_to_drop=DROP_FEATURES)),
    
    # add missing indicator in numerical 
    ('missing_indicator_num', AddMissingIndicator(variables=NUMERICAL_VARS_WITH_NA)),

    # impute numerical variables with the mean
    ('mean_imputation', MeanMedianImputer(imputation_method='mean', variables=NUMERICAL_VARS_WITH_NA)),
        
    # impute categorical  variables with frequent
    ('freq_imputation_cat', CategoricalImputer(imputation_method= "frequent", variables=CATEGORICAL_VARS_WITH_NA)),
    
    #  imputer, one-hot encoder
    ('onehot', OneHotEncoder(handle_unknown='ignore', sparse=False)),

    ('lr',LogisticRegression())  
])


# In[11]:


# train the pipeline
survived_pipe.fit(X_train, Y_train.values.ravel())


# In[12]:


#Get predictions
y_pred = survived_pipe.predict(X_test)

from sklearn.metrics import accuracy_score
# Evaluate the model
score = accuracy_score(Y_test, y_pred)

print("Score: {}".format(score))


# In[13]:


from sklearn.metrics import classification_report

print(classification_report(Y_test, y_pred))


# In[14]:


#Prueba del modelo
ejemplo = pd.DataFrame([[1, 'Allison, Miss. Helen Loraine', 'female', 2.0000, 1, 2, 113781, 151.55,'C22 C26','S']], 
            columns = ["pclass", "name", "sex", "age", "sibsp", "parch", "ticket","fare", "cabin", "embarked"])

pred_1 = survived_pipe.predict(ejemplo)
print(pred_1[0])


# In[16]:


#Se almacena el modelo en un pickle file. 
joblib.dump(survived_pipe, open('/home/nathalia//Documentos/analitica_escalable/bloque_2/my_model.pkl', 'wb'))


# In[23]:


file_name = "/home/nathalia/Documentos/analitica_escalable/bloque_2/my_model.pkl"
testing = pd.read_pickle(file_name)
print(testing)

