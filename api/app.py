from flask import Flask, request, jsonify, render_template, session, redirect, url_for, session
import requests
import pandas as pd
import numpy as np
from my_model.predict import make_prediction


app = Flask(__name__, template_folder='template')
@app.route('/',  methods = ['GET','POST'])
def home():
    if request.method == 'POST':  
        # Get values through input bars
        clas = request.form.get("class")
        name = request.form.get("name")
        sex = request.form.get("sex")
        age = request.form.get("age")
        sibsp = request.form.get("sibsp")
        parch = request.form.get("parch")
        fare = request.form.get("fare")
        embarked = request.form.get("embarked")

        print([clas, sex,age,sibsp,parch,fare,embarked])
        # Put inputs to dataframe
        X = pd.DataFrame([[clas, name, sex, age, sibsp, parch, None, fare, None, embarked]], columns = ["pclass","name", "sex","age","sibsp", "parch","ticket","fare","cabin","embarked"])
        # Get prediction
        prediction = make_prediction(X)

        if prediction == 0:
            prediction = "{} Survived".format(name)
        else:
            prediction = "{} Died".format(name)
    else:
        prediction = ""

    return render_template("website.html", output = prediction)


if __name__ =='__main__':
    app.run(debug=True,host="0.0.0.0", port=int("5000"))