### Comando docker para hacer pull del repositorio: 

(Solo el modelo)

docker pull nathifer/ml-titanic_survived:1


(Modelo y API)

docker pull nathifer/ml-titanic_survived:2 


### Breve Explicación :

Información de las variables que describen nuestro set de datos: 
- Pclass: La clase en la que viajó cada pasajero, tiene 3 valores posibles: Primera Clase (1) Segunda Clase(2) Tercera Clase(3) 
- Name: El nombre del pasajero Sex: Género del pasajero. 
- Age: Edad del pasajero. 
- SibSp: Número de hermanos/esposa(o), que viajaban con el pasajero 
- Parch: Número de padres e hijos que viajaban con el pasajero 
- Ticket: El número del boleto 
- Fare: La tarifa pagada por el boleto 
- Cabin: El número de compartimiento donde se alojó el pasajero 
- Embarked: Hace referencia al lugar donde el pasajero abordó el Titanic, tenemos 3 valores diferentes: 
		Southampton, Inglaterra(S) 
		Cherbourg, Francia(C) 
		Queenstown, Irlanda(Q)
- Survived, que será nuestra variable objetivo,  indica si una persona sobrevivió o murió. 

En un primer analisis podemos observar que existe informacion falante: 
 tres de las variables tienen valores nulos
- age         20.10%
- cabin       77.44%
- embarked     0.15%

Es por ello que decidimos realizar las siguientes Transformers y Estimator:

+ Se descartan la variable cabin por la gran cantidad de nulos que tiene ya que no aportaría información al modelo
+ Se descartan las variables name y ticket ya que no aportan informacion al modelo.
+ Se añaden las 3 variables en el DROP_FEATURES para canalizarla en el pipeline y que se eliminen

+ Utilizamos el Addminissing para comprobar los valores NA dentro de la variable "age" que posteriormente se imputa con la media de la edad utulizando el MeanMedianImputer.

+ Se imputan valores NA en la variable categorica "embarked" utilizando el valor mas frecuente mediante el CategoricalImputer 

+ Utilizamos el OneHotEncoder OHE para cambiar los numeros a una matriz para hacer las variables "binarias" en una categoría, poniendo el valor de 1 cuando la instancia de la variable ocurra y 0 en caso contrario.

+ Utilizamos un modelo de regresion logistica. 

Posteriormente realizamos el fit de nuestro modelo  y el predict que nos arroja un score de 79%
Se realiza una prueba del modelo con una de las lineas del dataset y predice correctamente. Por ultimo se almacena en un Pickle de python. 

### URL de Git con la información del DockerFile (y lo que se utilizó para crear la imagen)

+ Repositorio Git

https://gitlab.com/Nathifer/titanic_pec2/



+ Repositorio Docker

https://hub.docker.com/repository/docker/nathifer/ml-titanic_survived



+ tid-titanic 0.0.5

https://pypi.org/project/tid-titanic/

Comdando:   pip install tid-titanic

